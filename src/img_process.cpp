/**
 * @file
 * @brief Example node to visualize range, near ir and signal images
 *
 * Publishes ~/range_image, ~/nearir_image, and ~/signal_image.  Please bear
 * in mind that there is rounding/clamping to display 8 bit images. For computer
 * vision applications, use higher bit depth values in /os_cloud_node/points
 */

#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
#include <algorithm>

#include "ouster/client.h"
#include "ouster/image_processing.h"
#include "ouster/types.h"
#include "ouster_ros/OSConfigSrv.h"
#include "ouster_ros/ros.h"

#include <cv_bridge/cv_bridge.h>
#include <opencv2/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <Eigen/Dense>
#include <omp.h>

#include "milosMagic.hpp"

#define IMAGE_W 2048
#define IMAGE_H 128

#pragma omp declare reduction (merge : std::vector<int*> : omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end())) initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

namespace sensor = ouster::sensor;
namespace viz = ouster::viz;

using pixel_type = uint16_t;
const size_t pixel_value_max = std::numeric_limits<pixel_type>::max();

const int BRESENHAM_INDICES[][2] = {
  {-1, -3},
  {0, -3},
  {1, -3},
  {2, -2},
  {3, -1},
  {3, 0},
  {3, 1},
  {2, 2},
  {1, 3},
  {0, 3},
  {-1, 3},
  {-2, 2},
  {-3, 1},
  {-3, 0},
  {-3, -1},
  {-2, -2}
};

bool fastCompassCheck(Eigen::Map<ouster::img_t<pixel_type>> image, int u, int v, int FAST_THRESHOLD, int FAST_CHECK_COUNT){
    pixel_type centerI = image(u, v);

    pixel_type northI = image(u - 3, v);
    pixel_type southI = image(u + 3, v);
    pixel_type westI = image(u, v - 3);
    pixel_type eastI = image(u, v + 3);

    pixel_type diffN = abs(northI - centerI);
    pixel_type diffS = abs(southI - centerI);
    pixel_type diffW = abs(westI - centerI);
    pixel_type diffE = abs(eastI - centerI);

    int diffs = 0;
    diffs += diffN >= FAST_THRESHOLD ? 1 : 0;
    diffs += diffS >= FAST_THRESHOLD ? 1 : 0;
    diffs += diffW >= FAST_THRESHOLD ? 1 : 0;
    diffs += diffE >= FAST_THRESHOLD ? 1 : 0;

    return diffs >= FAST_CHECK_COUNT;
}


pixel_type goRoundBresenham(Eigen::Map<ouster::img_t<pixel_type>> image, int u, int v, int FAST_THRESHOLD, int FAST_COUNT){
  int corrDiffs = 0;
  pixel_type centerI = image(u, v);

  bool circleStarted = false;
  int i = 0;
  pixel_type corneriness = 0;
  
  int loops = 0;
  while (true) {
   int dv = BRESENHAM_INDICES[i][0], du = BRESENHAM_INDICES[i][1];
   pixel_type diff = abs(centerI - image(u + du, v + dv));
   if (diff >= FAST_THRESHOLD) {
     corrDiffs++;
     circleStarted = true;
     corneriness += diff - FAST_THRESHOLD;
   } else if (circleStarted) {
     return 0;
   }

   if (circleStarted && corrDiffs >= FAST_COUNT)
     return corneriness;
   i++;
   i %= 16;
    
    // UNNECESSARY BUT BETTER SAFE THAN SORRY (IN CASE U FORGOR FAST COMPASS CHECK)
   if (i == 0)
     loops++;
   if (loops == 2)
     return 0;
  }
}

std::vector<int*> customFAST(Eigen::Map<ouster::img_t<pixel_type>> image, int h, int w, int FAST_THRESHOLD, int FAST_COUNT, int FAST_CHECK_COUNT, bool nms = true) {
  std::vector<int*> keypoints;
  
  std::vector<std::vector<pixel_type>> cornerinessMatrix;
  for (int u = 0; u < h; u++) {
      std::vector<pixel_type> line(w, 0);
      cornerinessMatrix.push_back(line);
    }

 //#pragma omp parallel for reduction(merge: keypoints) schedule(dynamic, 8)
 //#pragma omp parallel for schedule(dynamic, 16)
  for (int u = 3; u < h - 3; u++){
    for (int v = 3; v < w - 3; v++){
     if (!fastCompassCheck(image, u, v, FAST_THRESHOLD, FAST_CHECK_COUNT))
       continue;
     pixel_type corneriness = goRoundBresenham(image, u, v, FAST_THRESHOLD, FAST_COUNT);
     
     cornerinessMatrix[u][v] = corneriness;
     if (!nms)
       keypoints.push_back(new int[2]{u,v});
    }
  }

  if (nms) {
 //#pragma omp parallel for reduction(merge: keypoints) schedule(dynamic, 8)
    for (int u = 3; u < h - 3; u++){
      for (int v = 3; v < w - 3; v++){
        pixel_type centerC = cornerinessMatrix[u][v];
        if (centerC <= 0)
          continue;

        bool maxima = true;
        for (int du = -1; du <= 1; du++)
          for (int dv = -1; dv <= 1; dv++)
            if (cornerinessMatrix[u+du][v+dv] > centerC)
              maxima = false;

        if (maxima){
          keypoints.push_back(new int[2]{u, v});
        }
      }
    }
  } 

  return keypoints;
}

std::vector<int*> generateBRIEFPattern(int w, int h, int len) {
  std::vector<int*> pattern(len, NULL);
  for (int i = 0; i < len; i++) {
    int* pairs = new int[4];
    pairs[0] = (rand() % w) - w/2; 
    pairs[1] = (rand() % h) - h/2; 
    pairs[2] = (rand() % w) - w/2; 
    pairs[3] = (rand() % h) - h/2; 
    
    pattern[i] = pairs;
  }
  return pattern;
}

std::vector<int*> customBRIEF(Eigen::Map<ouster::img_t<pixel_type>> image, std::vector<int*> features, std::vector<int*> patternBRIEF, int patternLen) {
  #define IS_IN_BOUNDS(u, v) ((u) >= 0 && (u) < IMAGE_H && (v) >= 0 && (v) < IMAGE_W)
  
  std::vector<int*> descriptors(features.size(), NULL);
  
  //int d = 0;
//#pragma omp parallel for reduction(merge: descriptors)
  for (size_t d = 0; d < features.size(); d++) {
  //for (int* ftr : features){
    int* ftr = features[d];
    int u = ftr[0];
    int v = ftr[1];
    
    descriptors[d] = new int[patternLen];
    int i = 0;
    for (int* pair : patternBRIEF) {
      int up1 = pair[0] + u;
      int vp1 = (pair[1] + v + IMAGE_W) % IMAGE_W;
      int up2 = pair[2] + u;
      int vp2 = (pair[3] + v + IMAGE_W) % IMAGE_W;
      
      if (!IS_IN_BOUNDS(up1,vp1) || !IS_IN_BOUNDS(up2, vp2)) {
        descriptors[d][i++] = -1;
        continue;
      }

      descriptors[d][i++] = image(up1, vp1) < image(up2, vp2) ? 1 : 0;
    }
    //d++;
  }
  return descriptors;
}


sensor_msgs::ImagePtr make_image_msg(size_t H, size_t W,
                                     const ros::Time& stamp) {
    sensor_msgs::ImagePtr msg{new sensor_msgs::Image{}};
    msg->width = W;
    msg->height = H;

    msg->step = W * sizeof(pixel_type);
    msg->encoding = sensor_msgs::image_encodings::MONO16;
    msg->data.resize(W * H * sizeof(pixel_type));
    msg->header.stamp = stamp;

    return msg;
}

bool arrContains(int arr[], int len, int val){
  for (int i = 0; i < len; i++)
    if (arr[i] == val)
      return true;
  return false;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "img_node");
    ros::NodeHandle nh("~");

    ouster_ros::OSConfigSrv cfg{};
    auto client = nh.serviceClient<ouster_ros::OSConfigSrv>("os_config");
    client.waitForExistence();
    if (!client.call(cfg)) {
        ROS_ERROR("Calling os config service failed");
        return EXIT_FAILURE;
    }

    auto info = sensor::parse_metadata(cfg.response.metadata);
    size_t H = info.format.pixels_per_column;
    size_t W = info.format.columns_per_frame;

    const auto& px_offset = info.format.pixel_shift_by_row;
    

//#define FAST_THRESHOLD 512
//#define FAST_COUNT 12
//#define FAST_CHECK_COUNT 3

//#define BRIEF_LEN 256
//#define BRIEF_THRESHOLD 32
//#define BRIEF_WIDTH 128
//#define BRIEF_HEIGHT 32

//#define PIXEL_THRESHOLD (32 * 32)
    
    if (!nh.hasParam("fastThreshold"))
      nh.setParam("fastThreshold", 512);
    if (!nh.hasParam("fastCount"))
      nh.setParam("fastCount", 12);    
    if (!nh.hasParam("fastCheckCount"))
      nh.setParam("fastCheckCount", 3);

    if (!nh.hasParam("briefLen"))
      nh.setParam("briefLen", 256);
    if (!nh.hasParam("briefThreshold"))
      nh.setParam("briefThreshold", 32);
    if (!nh.hasParam("briefWidth"))
      nh.setParam("briefWidth", 128);
    if (!nh.hasParam("briefHeight"))
      nh.setParam("briefHeight", 32);

    if (!nh.hasParam("pixelThreshold"))
      nh.setParam("pixelThreshold", 32 * 32);
    
    // mm^2 ? m^2 ? 
    if (!nh.hasParam("ransacThresholdError"))
      nh.setParam("ransacThresholdError", 512);
    
    if (!nh.hasParam("ransacMaxIter"))
      nh.setParam("ransacMaxIter", 128);
    
    // fraction ? t - n >= 5 as per orig. paper (n = 3 in our case)
    if (!nh.hasParam("ransacAcceptN"))
      nh.setParam("ransacAcceptN", 32);
    
    int FAST_THRESHOLD;
     nh.getParam("fastThreshold", FAST_THRESHOLD);
     int FAST_COUNT;
     nh.getParam("fastCount", FAST_COUNT);
     int FAST_CHECK_COUNT;
     nh.getParam("fastCheckCount", FAST_CHECK_COUNT);
    
    int BRIEF_LEN;
    nh.getParam("briefLen", BRIEF_LEN);
    int BRIEF_THRESHOLD;
    nh.getParam("briefThreshold", BRIEF_THRESHOLD);
    int BRIEF_WIDTH;
    nh.getParam("briefWidth", BRIEF_WIDTH);
    int BRIEF_HEIGHT; 
    nh.getParam("briefHeight", BRIEF_HEIGHT);
    
    int PIXEL_THRESHOLD; 
    nh.getParam("pixelThreshold", PIXEL_THRESHOLD);
    
    int RANSAC_THRESHOLD;
    nh.getParam("ransacThresholdError", RANSAC_THRESHOLD); 
    int RANSAC_MAX_ITER;
    nh.getParam("ransacMaxIter", RANSAC_MAX_ITER);
    int RANSAC_ACCEPT_N;
    nh.getParam("ransacAcceptN", RANSAC_ACCEPT_N);
    
    
    //for (auto po : px_offset)
    //  ROS_DEBUG("--------%d", po);

    ros::Publisher range_image_pub =
        nh.advertise<sensor_msgs::Image>("range_image", 100);
    ros::Publisher nearir_image_pub =
        nh.advertise<sensor_msgs::Image>("nearir_image", 100);
    ros::Publisher signal_image_pub =
        nh.advertise<sensor_msgs::Image>("signal_image", 100);
    ros::Publisher reflec_image_pub =
        nh.advertise<sensor_msgs::Image>("reflec_image", 100);

    ros::Publisher pcFeaturesPub =
        nh.advertise<sensor_msgs::PointCloud2>("point_cloud_features", 1);
    
    ros::Publisher odometryPub =
        nh.advertise<nav_msgs::Odometry>("odometry", 1);
    
    tf::TransformBroadcaster odometryBc;

    ouster_ros::Cloud cloud{};

    viz::AutoExposure nearir_ae, signal_ae, reflec_ae;
    viz::BeamUniformityCorrector nearir_buc;

    ouster::img_t<double> nearir_image_eigen(H, W);
    ouster::img_t<double> signal_image_eigen(H, W);
    ouster::img_t<double> reflec_image_eigen(H, W);
    
    const std::vector<int*> patternBRIEF = generateBRIEFPattern(BRIEF_WIDTH, BRIEF_HEIGHT, BRIEF_LEN);
     
    Eigen::Matrix3Xd featureCoordsPrev;
    std::vector<int*> descriptorsPrev;
    std::vector<int*> keypointsPrev;

    double cumYaw = 0;
    double cumPitch = 0;
    double cumRoll = 0;
    
    double cumX = 0;
    double cumY = 0;
    double cumZ = 0;

    auto cloud_handler = [&](const sensor_msgs::PointCloud2::ConstPtr& m) {
        pcl::fromROSMsg(*m, cloud);

        auto range_image = make_image_msg(H, W, m->header.stamp);
        auto nearir_image = make_image_msg(H, W, m->header.stamp);
        auto signal_image = make_image_msg(H, W, m->header.stamp);
        auto reflec_image = make_image_msg(H, W, m->header.stamp);

        // views into message data
        auto range_image_map = Eigen::Map<ouster::img_t<pixel_type>>(
            (pixel_type*)range_image->data.data(), H, W);
        auto nearir_image_map = Eigen::Map<ouster::img_t<pixel_type>>(
            (pixel_type*)nearir_image->data.data(), H, W);
        auto signal_image_map = Eigen::Map<ouster::img_t<pixel_type>>(
            (pixel_type*)signal_image->data.data(), H, W);
        auto reflec_image_map = Eigen::Map<ouster::img_t<pixel_type>>(
            (pixel_type*)reflec_image->data.data(), H, W);

        // copy data out of Cloud message, with destaggering
        for (size_t u = 0; u < H; u++) {
            for (size_t v = 0; v < W; v++) {
                const size_t vv = (v + W - px_offset[u]) % W;
                const auto& pt = cloud[u * W + vv];
                
                // 16 bit img: use 4mm resolution and throw out returns > 260m
                auto r = (pt.range + 0b10) >> 2;
                range_image_map(u, v) = r > pixel_value_max / 2 ? 0 : r;
                
                nearir_image_eigen(u, v) = pt.ambient;
                signal_image_eigen(u, v) = pt.intensity;
                reflec_image_eigen(u, v) = pt.reflectivity;
            }
        }
        // WE GO HEEEEEEEEEREEEEEEEEEEE
        auto keypointsPrefilter = customFAST(range_image_map, H, W, FAST_THRESHOLD, FAST_COUNT, FAST_CHECK_COUNT);
        std::vector<int*> keypoints;
        for (size_t i = 0; i < keypointsPrefilter.size(); i++){ 
          int* imgCoords = keypointsPrefilter[i];
          int u = imgCoords[0];
          int v = imgCoords[1];

          const int vv = (v + W - px_offset[u]) % W;
          const auto& pt = cloud[u * W + vv]; 

          if (pt.range > 300 && pt.range < 50000)
            keypoints.push_back(imgCoords);
        }

        ROS_DEBUG("Found %zd features!", keypoints.size());
        auto descriptors = customBRIEF(range_image_map, keypoints, patternBRIEF,  BRIEF_LEN);

        cv_bridge::CvImagePtr cv_ptr;
        try {
          cv_ptr = cv_bridge::toCvCopy(range_image, sensor_msgs::image_encodings::MONO16);
        } catch (cv_bridge::Exception& e){
          ROS_ERROR("cv_bridge exception: %s", e.what());
        }
        
        for (int* coords : keypoints)
          cv::circle(cv_ptr->image, cv::Point(coords[1], coords[0]), 10, cv::Scalar(255));
        
        range_image = cv_ptr->toImageMsg();
        
        // GET XYZ 
        //std::vector<int*> keypointCoords(keypoints.size(), NULL);
        
        
        Eigen::Matrix3Xd keypointCoords;
        keypointCoords.resize(3, keypoints.size());
        int i = 0;
        for (int* imgCoords : keypoints) {
          int u = imgCoords[0];
          int v = imgCoords[1];

          const int vv = (v + W - px_offset[u]) % W;
          const auto& pt = cloud[u * W + vv]; 
          //ROS_DEBUG("Range found vs img. range %d %d", (pt.range + 0b10) >> 2, range_image_map(u,v)); 
          //ROS_ASSERT(((pt.range + 0b10) >> 2) == range_image_map(u, v));
          //ROS_DEBUG("u: %d, v: %d, vv: %d", u, v, vv);
          // This is in meters. Just go with the flow... (cuz of 'range_unit' in ouster/types.h)
          //int* coords = new int[3];
          Eigen::Vector3d vec;
          vec(0) = pt.x * 1000;
          vec(1) = pt.y * 1000;
          vec(2) = pt.z * 1000;

          keypointCoords.col(i) = vec;
          i++;

         // ROS_DEBUG("Feature found at X: %f (%d), Y: %f (%d), Z: %f (%d) (Range:%d)", pt.x, vec[0],  pt.y, vec[1], pt.z, vec[2], pt.range);
        }
        
        ROS_DEBUG_STREAM(keypointCoords);


        ROS_DEBUG("Beginning Matching...");
        // Matching
        std::vector<int*> matchesOut;
        if (descriptorsPrev.size() != 0) {            
            int di = 0;
            std::vector<int*> matches;

            for (int* desc : descriptors) {
              int champDist = BRIEF_THRESHOLD;
              int champIdx = -1;

              int di2 = 0;
              for (int* desc2 : descriptorsPrev) {
                int dist = 0;
                for (int i = 0; i < BRIEF_LEN; i++) 
                  dist += desc[i] != desc2[i] ? 1 : 0;
                if (dist < champDist) {
                  champDist = dist;
                  champIdx = di2;
                }
                di2++;
              }
              
              // Matched
              if (champDist < BRIEF_THRESHOLD) {
                  if ( pow(keypoints[di][0] - keypointsPrev[champIdx][0], 2) + pow(keypoints[di][1] - keypointsPrev[champIdx][1], 2) < PIXEL_THRESHOLD) {        
                    if (keypointCoords.col(di).squaredNorm() > (300 * 300) && keypointCoords.col(di).squaredNorm() < (50000.0 * 50000)   
                        && featureCoordsPrev.col(champIdx).squaredNorm() > (300 * 300) && featureCoordsPrev.col(champIdx).squaredNorm() < (50000.0 * 50000)){ 
                      matches.push_back(new int[2]{di, champIdx});
                    } else {
                      ROS_DEBUG("Norms: %f %f", keypointCoords.col(di).norm(), featureCoordsPrev.col(champIdx).norm());
                    }
                  }
              }

              di++;
            }
            
            // RANSAC HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
            
            //A.resize(3, matches.size());
            //B.resize(3, matches.size());
            //A.resize(3, 3);
            //B.resize(3, 3);
            
            ROS_DEBUG("Beginning RANSAC...");
             
            Eigen::Matrix3Xd Afull;
            Eigen::Matrix3Xd Bfull;
            Afull.resize(3, matches.size());
            Bfull.resize(3, matches.size());
            
            ROS_DEBUG("Matches %zd", matches.size());
            //ROS_DEBUG("fulls: %zd %zd %zd %zd", Afull.rows(), Afull.cols(), Bfull.rows(), Bfull.cols());
            
            int i = 0;
            for (int* match : matches) {
              Bfull.col(i) = keypointCoords.col(match[0]);
              Afull.col(i) = featureCoordsPrev.col(match[1]);

              i++;
            }
            
            Eigen::Matrix3d Rbest = Eigen::Matrix3d::Identity();
            Eigen::Vector3d Tbest = Eigen::Vector3d::Zero();
            
            size_t inliersSizeChamp = 0;
            for (int ransacIter = 0; ransacIter < RANSAC_MAX_ITER && matches.size() >= 3; ransacIter++) {
              int maxSampleSize = (int)std::min((int)matches.size() / 8, 8) + 1;
              int sampleSize = (rand() % maxSampleSize) + 3;
              
              Eigen::Matrix3Xd A;
              Eigen::Matrix3Xd B;
              A.resize(3, sampleSize);
              B.resize(3, sampleSize);

              //ROS_DEBUG("Matches: %zd, Sample Size: %d", matches.size(), sampleSize);
               // Random Sample.
              std::vector<int> randomSample(sampleSize, -1); // MAKE SURE THERE ARE NO DUPLICATES!!!!!!!!!!
                    
              for (int i = 0; i < sampleSize; i++){
                int randomSampleIdx = -2;
                do {
                    randomSampleIdx = rand() % matches.size();
                } while (std::find(randomSample.begin(), randomSample.end(), randomSampleIdx)
                          != randomSample.end());
                
                randomSample[i] = randomSampleIdx;
                B.col(i) = keypointCoords.col(matches[randomSample[i]][0]);
                A.col(i) = featureCoordsPrev.col(matches[randomSample[i]][1]);
              }
              //ROS_DEBUG("Random Samples: %d %d %d", randomSample[0], randomSample[1],randomSample[2]);
              /*int i = 0;
              for (int* match : matches) {
                B.col(i) = keypointCoords.col(match[0]);
                A.col(i) = featureCoordsPrev.col(match[1]);

                i++;
              }*/
              
              //ROS_DEBUG(":::::::::::::::%zd %zd", A.rows(), A.cols());


              Eigen::Vector3d meanA = A.rowwise().sum() / A.cols();
              Eigen::Vector3d meanB = B.rowwise().sum() / B.cols();
              
              Eigen::Matrix3Xd Ac = A.colwise() - meanA;
              Eigen::Matrix3Xd Bc = B.colwise() - meanB;

              Eigen::Matrix3d H = Ac * (Bc.transpose());
              Eigen::JacobiSVD<Eigen::Matrix3d> svd(H, Eigen::ComputeFullV | Eigen::ComputeFullU);

              Eigen::Matrix3d R = svd.matrixV() * svd.matrixU().transpose();

              Eigen::Matrix3d Rfixed;
              // det == -1
              if (abs(R.determinant() + 1) < 0.001) {
                if (svd.singularValues()(0) != 0 && svd.singularValues()(1) != 0 && svd.singularValues()(2) != 0) {
                  ROS_DEBUG("Degenerate case found by SVD!");
                  continue; // SAFE ??
                }
                Eigen::Matrix3d V = svd.matrixV();
                V.col(2) *= -1;
                Rfixed = V * svd.matrixU().transpose();
              } else
                Rfixed = R;
              
              ROS_ASSERT(abs(Rfixed.determinant() - 1) < 0.001);
              Eigen::Vector3d T = meanB - (Rfixed * meanA);
              
              //ROS_DEBUG("Afull: %zd %zd, Bfull: %zd %zd, A: %zd %zd, B: %zd %zd", Afull.rows(), Afull.cols(), Bfull.rows(), Bfull.cols(), A.rows(), A.cols(), B.rows(), B.cols());
              
              //ros::Duration(0.5).sleep();
              Eigen::Matrix3Xd Best = (Rfixed * Afull).colwise() + T; // B estimate...
              Eigen::Matrix3Xd tmp = (Bfull - Best).array().pow(2).matrix();
              Eigen::Matrix<double, 1, Eigen::Dynamic> pointwiseError = tmp.colwise().sum();
              //ROS_DEBUG("Pointwise Error dims: %zd %zd", pointwiseError.rows(), pointwiseError.cols());
              //ROS_DEBUG("Best: %zd %zd, tmp: %zd %zd", Best.rows(), Best.cols(), tmp.rows(), tmp.cols());
              
              std::vector<int> inliers; // indices of inliers! See RANSAC for moar info
              for (int i = 0; i < pointwiseError.cols(); i++)
                if (pointwiseError(0, i) < (double)RANSAC_THRESHOLD)
                  inliers.push_back(i);
                
             
              ROS_DEBUG("Inliers %zd", inliers.size());             
              //long long unsigned int errorSq = pointwiseError.sum(); // NOT LIKE THIS U IDIOT!!!!!!!!!!!!!!!
              if (inliers.size() > inliersSizeChamp && inliers.size() > (size_t) RANSAC_ACCEPT_N) {
                inliersSizeChamp = inliers.size();
                Rbest = Rfixed;
                Tbest = T;
                
                ROS_DEBUG_STREAM("B: " << Bfull);
                ROS_DEBUG_STREAM("Best: " << Best);
                //ROS_DEBUG_STREAM("PWErr: " << pointwiseError);
              ROS_DEBUG("Inliers: %zd (%f %f %f %f)", inliers.size(), pointwiseError.coeff(0, inliers[0]), pointwiseError.coeff(0, inliers[1]), pointwiseError.coeff(0, inliers[2]),pointwiseError.coeff(0, inliers[3]));
              ROS_DEBUG("Inliers Idx: %d %d %d %d", inliers[0], inliers[1], inliers[2], inliers[3]);
                //if ((int) inliers.size() > RANSAC_ACCEPT_N)
              ROS_DEBUG_STREAM("B Best cols: " << Afull.col(inliers[0]) << "\n" << Afull.col(inliers[1]) << "\n" << Afull.col(inliers[2]) << "\n" << Afull.col(inliers[3]) << "Best: " << Best.col(inliers[0]) << "\n" << Best.col(inliers[1]) << "\n" << Best.col(inliers[2]) << "\n" << Best.col(inliers[3]) );
                //  break;
              }
            }



            for (size_t i = 0; i < matchesOut.size(); i++)
              delete matchesOut[i];


            double yaw = atan2(Rbest(1, 0), Rbest(0, 0));
            double pitch = atan2(-Rbest(2, 0), sqrt(pow(Rbest(2, 1), 2) + pow(Rbest(2, 2), 2)));
            double roll = atan2(Rbest(2, 1), Rbest(2, 2));
            

            cumYaw += yaw;
            cumPitch += pitch;
            cumRoll += roll;
            
            double cumYawDeg = (cumYaw * 180) / M_PI;
            double cumPitchDeg = (cumPitch * 180) / M_PI;
            double cumRollDeg = (cumRoll * 180) / M_PI;


            cumX += Tbest.x();
            cumY += Tbest.y();
            cumZ += Tbest.z();

            ROS_INFO("T: %f %f %f; Yaw: %f, Pitch: %f, Roll: %f", cumX, cumY, cumZ, cumYawDeg, cumPitchDeg, cumRollDeg);
            
            //ros::Time currentTime = ros::Time::now();

            geometry_msgs::Quaternion odometryQuat = tf::createQuaternionMsgFromRollPitchYaw(cumRoll, cumPitch, cumYaw);
            geometry_msgs::TransformStamped odomTr;
            //odomTr.header.stamp = currentTime;
            odomTr.header.stamp = m->header.stamp;
            odomTr.header.frame_id = "odom";
            odomTr.child_frame_id = "os_sensor";

            odomTr.transform.translation.x = cumX / 1000;
            odomTr.transform.translation.y = cumY / 1000;
            odomTr.transform.translation.z = cumZ / 1000;
            odomTr.transform.rotation = odometryQuat;

            odometryBc.sendTransform(odomTr);

            nav_msgs::Odometry odom;
            //odom.header.stamp = currentTime;
            odom.header.stamp = m->header.stamp;
            odom.header.frame_id = "odom";

            odom.pose.pose.position.x = cumX / 1000;
            odom.pose.pose.position.y = cumY / 1000;
            odom.pose.pose.position.z = cumZ / 1000;
            odom.pose.pose.orientation = odometryQuat;
            
            odom.child_frame_id = "os_sensor";
            odom.twist.twist.linear.x = Tbest.x() / 1000;
            odom.twist.twist.linear.y = Tbest.y() / 1000;
            odom.twist.twist.linear.z = Tbest.z() / 1000;

            odom.twist.twist.angular.x = roll;        
            odom.twist.twist.angular.y = pitch;        
            odom.twist.twist.angular.z = yaw;        
            
            odometryPub.publish(odom);

            matchesOut = matches;
        }
        
        for (size_t i = 0; i < descriptorsPrev.size(); i++)
          delete descriptorsPrev[i];

        for (size_t i = 0; i < keypointsPrev.size(); i++)
          delete keypointsPrev[i];

        descriptorsPrev = descriptors;
        featureCoordsPrev = keypointCoords;
        keypointsPrev = keypoints;
        // image processing
        nearir_buc(nearir_image_eigen);
        nearir_ae(nearir_image_eigen);
        signal_ae(signal_image_eigen);
        reflec_ae(reflec_image_eigen);
        nearir_image_eigen = nearir_image_eigen.sqrt();
        signal_image_eigen = signal_image_eigen.sqrt();

        // copy data into image messages
        nearir_image_map =
            (nearir_image_eigen * pixel_value_max).cast<pixel_type>();
        signal_image_map =
            (signal_image_eigen * pixel_value_max).cast<pixel_type>();
        reflec_image_map =
            (reflec_image_eigen * pixel_value_max).cast<pixel_type>();

        // publish
        range_image_pub.publish(range_image);
        nearir_image_pub.publish(nearir_image);
        signal_image_pub.publish(signal_image);
        reflec_image_pub.publish(reflec_image);
        
        //milos structures...
        std::vector<Eigen::VectorXd> xyz;
        std::vector<uint8_t> values(keypointCoords.cols(), 42);
        for (int i = 0; i < keypointCoords.cols(); i++)
          xyz.push_back(keypointCoords.col(i) * 0.001);
        
        //for (size_t i = 0; i < matchesOut.size(); i++)
        //  xyz.push_back(keypointCoords.col(matchesOut[i][0]) * 0.001);
        
        auto pc2Msg = rds_cost_tools::value_2_pc2(&xyz, &values);
        
        pc2Msg.header = m->header; 
        //pc2Msg.header.frame_id = "/os_sensor";
        
        pcFeaturesPub.publish(pc2Msg);
    };

    auto pc_sub =
        nh.subscribe<sensor_msgs::PointCloud2>("points", 1, cloud_handler);

    ros::spin();
    return EXIT_SUCCESS;
}
