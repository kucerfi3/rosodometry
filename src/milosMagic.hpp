#ifndef rds_cost_tools_convert_2_msg_H
#define rds_cost_tools_convert_2_msg_H

#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>
#include <boost/format.hpp>

namespace rds_cost_tools
{

sensor_msgs::PointCloud2 value_2_pc2(std::vector<Eigen::VectorXd> * xyz, std::vector<uint8_t> * values, std::string name = "value");
}

#endif
