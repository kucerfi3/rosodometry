#ifndef rds_cost_tools_convert_2_msg_H
#define rds_cost_tools_convert_2_msg_H

#include <Eigen/Dense>
#include <vector>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>
#include <boost/format.hpp>

namespace rds_cost_tools
{

sensor_msgs::PointCloud2 value_2_pc2(std::vector<Eigen::VectorXd> * xyz, std::vector<uint8_t> * values, std::string name = "value")
{
    //printf("%d %d\n",xyz->size(),values->size());

    assert (xyz->size() == values->size());

		sensor_msgs::PointCloud2 pc2msg;

    //generate x field for rviz
    sensor_msgs::PointField pf_x;
    pf_x.name = "x";
    pf_x.datatype = 7;
    pf_x.offset = 0;
    pf_x.count = 1;
    //generate y field for rviz
    sensor_msgs::PointField pf_y;
    pf_y.name = "y";
    pf_y.datatype = 7;
    pf_y.offset = 4;
    pf_y.count = 1;
    //generate z field for rviz
    sensor_msgs::PointField pf_z;
    pf_z.name = "z";
    pf_z.datatype = 7;
    pf_z.offset = 8;
    pf_z.count = 1;
    //generate intensity field for rviz
    sensor_msgs::PointField pf_intensity;
    pf_intensity.name = name+"_vis";
    pf_intensity.datatype = 7;
    pf_intensity.offset = 12;
    pf_intensity.count = 1;
    //generate terrain type
    sensor_msgs::PointField pf_val;
    pf_val.name = name;
    pf_val.datatype = 2;
    pf_val.offset = 16;
    pf_val.count = 1;

    pc2msg.fields.push_back(pf_x); 
    pc2msg.fields.push_back(pf_y); 
    pc2msg.fields.push_back(pf_z); 
    pc2msg.fields.push_back(pf_intensity); 
    pc2msg.fields.push_back(pf_val); 

    pc2msg.point_step = 4*4+1;
    pc2msg.row_step = xyz->size()*pc2msg.point_step;
    pc2msg.height = 1;
    pc2msg.width = xyz->size();

    std::vector<uint8_t>::iterator it_value = values->begin();
    for (std::vector<Eigen::VectorXd>::iterator it = xyz->begin(); it < xyz->end(); ++it)
    {
      float x_val = (*it)(0);
      for (uint8_t * byteptr = (uint8_t *) &x_val; byteptr < ((uint8_t *) &x_val) + 4; byteptr++) pc2msg.data.push_back(*byteptr);
      float y_val = (*it)(1);
      for (uint8_t * byteptr = (uint8_t *) &y_val; byteptr < ((uint8_t *) &y_val) + 4; byteptr++) pc2msg.data.push_back(*byteptr);
      float z_val = (*it)(2);
      for (uint8_t * byteptr = (uint8_t *) &z_val; byteptr < ((uint8_t *) &z_val) + 4; byteptr++) pc2msg.data.push_back(*byteptr);
      uint8_t val_val = (*it_value);
      float vis_val = (float)val_val;
      for (uint8_t * byteptr = (uint8_t *) &vis_val; byteptr < ((uint8_t *) &vis_val) + 4; byteptr++) pc2msg.data.push_back(*byteptr);
      for (uint8_t * byteptr = (uint8_t *) &val_val; byteptr < ((uint8_t *) &val_val) + 1; byteptr++) pc2msg.data.push_back(*byteptr);
      ++it_value;
    }

    return pc2msg;
}

}

#endif
